package com.foreveross.bsl.push.repository.custom;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.foreveross.bsl.push.domain.entity.DeviceCheckin;

public interface DeviceCheckinRepositoryCustom {
	List<DeviceCheckin> findByTag(String tagKey, String tagValue, String appId);
	List<DeviceCheckin> findByTags(Map<String, String> tags, String appId);
	Page<DeviceCheckin> findByTags(Map<String, String> tags, String appId, Pageable pageable);
	boolean existsByTags(Map<String, String> tags, String appId);
	int clearToken(String token);
}
