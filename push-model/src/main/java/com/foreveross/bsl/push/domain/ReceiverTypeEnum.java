package com.foreveross.bsl.push.domain;

public enum ReceiverTypeEnum {
	//指定的设备。
	DEVICE_ID(1),
	
	//指定的 tag。
	TAG(2),
	
	//指定的 alias。
	ALIAS(3),
	
	//对指定appId的所有用户推送消息。
	APP_ID(4);
	
	private final int value;
	private ReceiverTypeEnum(final int value) {
		this.value = value;
	}
	public int value() {
		return this.value;
	}
}
