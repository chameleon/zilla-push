package com.foreveross.bsl.push.domain.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.foreveross.bsl.mongodb.domain.Entity;

@JsonIgnoreProperties({"entityRepositoryClass","entityRepository"})
@Document
public class Push extends Entity {

	public enum PushStatusEnum {
		PENDING, SENDING, SENT, OFFLINE, FAILURE, RECEIPT, CANCEL
	}

	@Id
	private String id;

	@Field
	private PushTarget target;

	@Field
	private String channelId;

	/**
	 * 保存离线的时长。秒为单位。最多支持10天（864000秒）。 0 表示该消息不保存离线。即：用户在线马上发出，当前不在线用户将不会收到此消息。
	 * 此参数不设置则表示默认，默认为保存1天的离线消息（86400秒）。
	 */
	private long timeToLive = 86400;

	@Field("create_time")
	private Date createTime;
	
	@Field("sendingTime")
	private Date sendingTime;
	
	@Field("sentTime")
	private Date sentTime;

	@Field("expired_time")
	private Date expiredTime;

	@Field("receipt_time")
	private Date receiptTime;

	@Field
	private PushStatusEnum status;

	@Field("retry_cnt")
	private int retryCount;

	@Field
	private String lastErrorMsg;

	@DBRef
	private PushRequest request;

	/**
	 * 实际处理发送时的发送批次
	 */
	@Field("sendId")
	private String sendId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public PushTarget getTarget() {
		return target;
	}

	public void setTarget(PushTarget target) {
		this.target = target;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getReceiptTime() {
		return receiptTime;
	}

	public void setReceiptTime(Date receiptTime) {
		this.receiptTime = receiptTime;
	}

	public PushStatusEnum getStatus() {
		return status;
	}

	public void setStatus(PushStatusEnum status) {
		this.status = status;
	}

	public int getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}

	public String getLastErrorMsg() {
		return lastErrorMsg;
	}

	public void setLastErrorMsg(String lastErrorMsg) {
		this.lastErrorMsg = lastErrorMsg;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public long getTimeToLive() {
		return timeToLive;
	}

	public void setTimeToLive(long timeToLive) {
		this.timeToLive = timeToLive;
	}

	/**
	 * @return the expiredTime
	 */
	public Date getExpiredTime() {
		return expiredTime;
	}

	/**
	 * @param expiredTime
	 *            the expiredTime to set
	 */
	public void setExpiredTime(Date expiredTime) {
		this.expiredTime = expiredTime;
	}

	/**
	 * @return the sendId
	 */
	public String getSendId() {
		return sendId;
	}

	/**
	 * @param sendId
	 *            the sendId to set
	 */
	public void setSendId(String sendId) {
		this.sendId = sendId;
	}

	/**
	 * @return the request
	 */
	public PushRequest getRequest() {
		return request;
	}

	/**
	 * @param request the request to set
	 */
	public void setRequest(PushRequest request) {
		this.request = request;
	}

	/**
	 * @return the sentTime
	 */
	public Date getSentTime() {
		return sentTime;
	}

	/**
	 * @param sentTime the sentTime to set
	 */
	public void setSentTime(Date sentTime) {
		this.sentTime = sentTime;
	}

	/**
	 * @return the sendingTime
	 */
	public Date getSendingTime() {
		return sendingTime;
	}

	/**
	 * @param sendingTime the sendingTime to set
	 */
	public void setSendingTime(Date sendingTime) {
		this.sendingTime = sendingTime;
	}

}
