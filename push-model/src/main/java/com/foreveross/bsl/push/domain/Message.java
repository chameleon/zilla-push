package com.foreveross.bsl.push.domain;

import java.util.HashMap;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Field;

/**
 * 消息表示类
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-7-12
 *
 */
public class Message {
	//TODO 待重构,推送消息的定义须分离以下业务的语义
	public enum MessageTypeEnum{
		/**
		 * 系统消息，即不限定终端应用上的某个模块，只在“消息盒子”里查看
		 */
		SYS,
		/**
		 * 模块消息，针对某个特定的模块，模块图标上会显示未读消息条数，“消息盒子”仅看到标题
		 */
		MODULE, 
		/**
		 * 设备控制消息，在终端不作显示，用于处理控制设备
		 */
		MDM,
		/**
		 * 安全策略变更消息
		 */
		SECURITY
	}
	
	/**
	 * 等同pushRequest.id
	 */
	@Field
	private String id;
	
	/**
	 * 消息标题
	 */
	@Field
	private String title = "";
	
	/**
	 * 消息内容
	 */
	@Field
	private String content = "";
	
	/**
	 * 消息类型
	 */
	@Field
	private MessageTypeEnum messageType = MessageTypeEnum.SYS;
	
	/**
	 * 更多的附属信息，客户端根据需求添加自己的数据
	 */
	@Field
	private Map<String, Object> extras = new HashMap<String, Object>();
	
	/**
	 * 需要直接发送到推送渠道的extras属性，默认是不发送任何extras属性
	 */
	private String[] directExtrasPropertys;

	public Message() {}

	public Message(String title, String content) {
		super();
		this.title = title;
		this.content = content;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Map<String, Object> getExtras() {
		return extras;
	}

	public void setExtras(Map<String, Object> extras) {
		this.extras = extras;
	}
	
	public Message putExtra(String key, Object obj){
		this.extras.put(key, obj);
		return this;
	}
	
	public Message removeExtra(String key){
		this.extras.remove(key);
		return this;
	}
	
	public Object getExtra(String key){
		return this.extras.get(key);
	}
	
	public boolean containsExtra(String key){
		return this.extras.containsKey(key);
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	/**
	 * @return the messageType
	 */
	public MessageTypeEnum getMessageType() {
		return messageType;
	}

	/**
	 * @param messageType the messageType to set
	 */
	public void setMessageType(MessageTypeEnum messageType) {
		this.messageType = messageType;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the directExtrasPropertys
	 */
	public String[] getDirectExtrasPropertys() {
		return directExtrasPropertys;
	}

	/**
	 * @param directExtrasPropertys the directExtrasPropertys to set
	 */
	public void setDirectExtrasPropertys(String[] directExtrasPropertys) {
		this.directExtrasPropertys = directExtrasPropertys;
	}

}
