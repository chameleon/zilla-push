/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.repository;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.foreveross.bsl.push.BaseTestWithSpringIoc;
import com.foreveross.bsl.push.domain.entity.TagSummary;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-8-31
 *
 */
public class TagSummaryRepoTest extends BaseTestWithSpringIoc{

	@Autowired
	private TagSummaryRepository tagRepo;
	
	@Test
	public void testUpdateAndRemoveTagSummary() {
		String appId="com.xxx.abc";
		try{
			//测试增加和更新
			tagRepo.updateTags(appId, "location", "广州", "深圳");
			TagSummary tagSum=tagRepo.findOne(appId);
			Assert.assertNotNull(tagSum);
			Assert.assertEquals(1, tagSum.getTags().size());
			String[] tagValues=tagSum.getTagSet("location").getValues();
			Assert.assertEquals(2, tagValues.length);
			Assert.assertTrue(ArrayUtils.contains(tagValues, "广州"));
			Assert.assertTrue(ArrayUtils.contains(tagValues, "深圳"));
			
			tagRepo.updateTags(appId, "level", "gold");
			tagSum=tagRepo.findOne(appId);
			Assert.assertEquals(2, tagSum.getTags().size());
			tagValues=tagSum.getTagSet("level").getValues();
			Assert.assertEquals(1, tagValues.length);
			Assert.assertTrue(ArrayUtils.contains(tagValues, "gold"));
			
			tagRepo.updateTags(appId, "level", "diamond");
			tagSum=tagRepo.findOne(appId);
			tagValues=tagSum.getTagSet("level").getValues();
			Assert.assertEquals(2, tagValues.length);
			Assert.assertTrue(ArrayUtils.contains(tagValues, "gold"));
			Assert.assertTrue(ArrayUtils.contains(tagValues, "diamond"));
			
			//测试删除
			tagRepo.removeTags(appId, "level", "diamond");
			tagSum=tagRepo.findOne(appId);
			tagValues=tagSum.getTagSet("level").getValues();
			Assert.assertEquals(1, tagValues.length);
			Assert.assertTrue(ArrayUtils.contains(tagValues, "gold"));
			//删除level的diamond
			Assert.assertFalse(ArrayUtils.contains(tagValues, "diamond"));
			tagValues=tagSum.getTagSet("location").getValues();
			Assert.assertTrue(ArrayUtils.contains(tagValues, "广州"));
			Assert.assertTrue(ArrayUtils.contains(tagValues, "深圳"));
			
			//删除level标签
			tagRepo.removeTags(appId, "level");
			tagSum=tagRepo.findOne(appId);
			Assert.assertNull(tagSum.getTagSet("level"));
			Assert.assertNotNull(tagSum.getTagSet("location"));
			
			//删除location的"广州"，"深圳"
			tagRepo.removeTags(appId, "location", "广州", "深圳");
			tagSum=tagRepo.findOne(appId);
			Assert.assertNotNull(tagSum.getTagSet("location"));
			Assert.assertEquals(0, tagSum.getTagSet("location").getValues().length);
			
			//删除所有标签
			tagRepo.removeAllTags(appId);
			tagSum=tagRepo.findOne(appId);
			Assert.assertNull(tagSum.getTags());
			
			tagRepo.updateTags(appId, "location", "广州", "深圳", "株洲");
			//删除时使用无效值
			tagRepo.removeTags(appId, "location", "XXX", "深圳");
			tagSum=tagRepo.findOne(appId);
			Assert.assertNotNull(tagSum.getTagSet("location"));
			tagValues=tagSum.getTagSet("location").getValues();
			Assert.assertEquals(2, tagValues.length);
			Assert.assertTrue(ArrayUtils.contains(tagValues, "广州"));
			Assert.assertTrue(ArrayUtils.contains(tagValues, "株洲"));
		}
		finally{
			tagRepo.delete(appId);
		}
	}
	
	@Test
	public void testRemoveTagForAllApp(){
		String appId="com.xxx.abc", appId1="aaa", appId2="bbb";
		try{
			tagRepo.updateTags(appId, "role", "机长", "管理员", "空姐");
			tagRepo.updateTags(appId1, "role", "机长", "管理员", "空姐");
			tagRepo.updateTags(appId2, "role", "机长", "管理员", "空姐");
			
			TagSummary tagSum=tagRepo.findOne(appId);
			Assert.assertEquals(3, tagSum.getTagSet("role").getValues().length);
			tagSum=tagRepo.findOne(appId1);
			Assert.assertEquals(3, tagSum.getTagSet("role").getValues().length);
			tagSum=tagRepo.findOne(appId2);
			Assert.assertEquals(3, tagSum.getTagSet("role").getValues().length);
			
			this.tagRepo.removeTagsForAllApps("role", "机长");
			tagSum=tagRepo.findOne(appId);
			Assert.assertEquals(2, tagSum.getTagSet("role").getValues().length);
			tagSum=tagRepo.findOne(appId1);
			Assert.assertEquals(2, tagSum.getTagSet("role").getValues().length);
			tagSum=tagRepo.findOne(appId2);
			Assert.assertEquals(2, tagSum.getTagSet("role").getValues().length);
			
			this.tagRepo.removeTagsForAllApps("role");
			tagSum=tagRepo.findOne(appId);
			Assert.assertEquals(0, tagSum.getTags().size());
			tagSum=tagRepo.findOne(appId1);
			Assert.assertEquals(0, tagSum.getTags().size());
			tagSum=tagRepo.findOne(appId2);
			Assert.assertEquals(0, tagSum.getTags().size());
		}
		finally{
			tagRepo.delete(appId);
			tagRepo.delete(appId1);
			tagRepo.delete(appId2);
		}
	}

}
