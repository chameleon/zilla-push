package com.foreveross.bsl.push.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Collection;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.foreveross.bsl.push.BaseTestWithSpringIoc;
import com.foreveross.bsl.push.domain.entity.Push;
import com.foreveross.bsl.push.domain.entity.Push.PushStatusEnum;
import com.foreveross.bsl.push.domain.entity.PushTarget;
import com.foreveross.bsl.push.repository.PushRepository;
import com.google.common.collect.Lists;

public class TestPushEntity extends BaseTestWithSpringIoc {

	@Autowired
	private PushRepository pushRepo;

	@Test
	public void testCRUD() {
		String deviceId = "ffffffff-8f4c-1da2-ffff-ffffa033aaeb", token = "ffffffff-8f4c-1da2-ffff-ffffa033aaeb@snda-192-168-2-32", channelId = "Openfire", appId = "com.foreveross.chameleon";
		Push push = new Push();
		push.setCreateTime(new Date());
		push.setStatus(PushStatusEnum.PENDING);
		push.setTarget(new PushTarget(deviceId, appId, token, "test"));
		push.setChannelId(channelId);
		pushRepo.save(push);
		push = this.pushRepo.findOne(push.getId());
		assertNotNull(push);
		assertEquals(PushStatusEnum.PENDING, push.getStatus());

		push.setStatus(PushStatusEnum.SENDING);
		pushRepo.save(push);
		push = this.pushRepo.findOne(push.getId());
		assertEquals(PushStatusEnum.SENDING, push.getStatus());

		pushRepo.delete(push);
		push = this.pushRepo.findOne(push.getId());
		assertNull(push);
	}

	@Test
	public void testFindAndUpdateToSendingByIds(){
		String pushId="52034625f979477756f6979a";
		Collection<String> pushIds=Lists.newArrayList(pushId);
		Push p=pushRepo.findOne(pushId);
		Assert.assertNotNull(p);
		Assert.assertEquals(Push.PushStatusEnum.PENDING, p.getStatus());
		
		pushRepo.findAndUpdateToSendingByIds(new Date(), pushIds);
		p=pushRepo.findOne(pushId);
		Assert.assertNotNull(p);
		Assert.assertEquals(Push.PushStatusEnum.SENDING, p.getStatus());
	}
}
