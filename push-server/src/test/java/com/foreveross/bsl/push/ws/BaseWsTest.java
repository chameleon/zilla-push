package com.foreveross.bsl.push.ws;
/**
 * Copyright (C) 2013-2014 the original author or authors.
 */


import java.util.List;

import org.junit.BeforeClass;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.foreveross.bsl.common.utils.ws.jackson.mixins.OrderMixin;
import com.foreveross.bsl.common.utils.ws.jackson.mixins.PageMixin;
import com.foreveross.bsl.common.utils.ws.jackson.mixins.PageRequestMixin;
import com.foreveross.bsl.common.utils.ws.jackson.mixins.PageableMixin;
import com.foreveross.bsl.common.utils.ws.jackson.mixins.SortMixin;
import com.google.common.collect.Lists;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-7-26
 *
 */
public abstract class BaseWsTest {
	
	protected static ObjectMapper mapper;
	protected static List<?> providers;
	
	@BeforeClass
	public static final void setupJacksonProvider(){
		mapper=new ObjectMapper();
		mapper.addMixInAnnotations(Page.class, PageMixin.class);
		mapper.addMixInAnnotations(Pageable.class, PageableMixin.class);
		mapper.addMixInAnnotations(PageRequest.class, PageRequestMixin.class);
		mapper.addMixInAnnotations(Sort.class, SortMixin.class);
		mapper.addMixInAnnotations(Sort.Order.class, OrderMixin.class);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		JacksonJaxbJsonProvider jacksonProvider=new JacksonJaxbJsonProvider();
		jacksonProvider.setMapper(mapper);
		providers=Lists.newArrayList(jacksonProvider);
	}

	public abstract String getTargetUrl();
	
}
