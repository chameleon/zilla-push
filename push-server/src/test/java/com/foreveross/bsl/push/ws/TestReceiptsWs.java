/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.ws;

import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.junit.Before;

import com.foreveross.bsl.push.application.ReceiptService;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-8-23
 *
 */
public class TestReceiptsWs extends BasePushWsTest{

	private ReceiptService receiptService;
	
	@Before
	public void setup(){
		receiptService = JAXRSClientFactory.create(this.getTargetUrl(), ReceiptService.class, providers);
	}
	
}
