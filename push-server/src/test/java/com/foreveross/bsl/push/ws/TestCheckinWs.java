package com.foreveross.bsl.push.ws;
/**
 * Copyright (C) 2013-2014 the original author or authors.
 */


import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.client.WebClient;
import org.junit.Before;
import org.junit.Test;

import com.foreveross.bsl.push.application.CheckinService;
import com.foreveross.bsl.push.application.vo.DeviceCheckinVo;
import com.foreveross.bsl.push.application.vo.TagEntryVo;

/**
 * xxx
 * 
 * @author Wangyi
 * @version v1.0
 * 
 * @date 2013-7-11
 * 
 */
public class TestCheckinWs extends BasePushWsTest{
	
	private CheckinService checkinService;
	
	@Before
	public void setup(){
		checkinService = JAXRSClientFactory.create(this.getTargetUrl(), CheckinService.class, providers);
	}

	@Test
	public void testCheckinUseProxyClient() {
		String deviceId = "ios_test_device1", appId = "fdsfdsfewo", alias = "wangyi";
		DeviceCheckinVo checkinVo = new DeviceCheckinVo();
		checkinVo.setDeviceId(deviceId);
		checkinVo.setAppId(appId);
		checkinVo.setAlias(alias);
		checkinVo.setChannelId("apns_sandbox");
		checkinVo.setDeviceName("iPad Mini");
		checkinVo.setGis("128,68");
		checkinVo.setOsName("IOS");
		checkinVo.setOsVersion("10");
		checkinVo.setPushToken("D189D92D69CDA67823C1A4AF2FDD66215B53539603B1F3E136A950DE3D362EB9");
		checkinVo.setTags(new TagEntryVo[] { new TagEntryVo("location", "gz"), 
				new TagEntryVo("level", "gold"),
				new TagEntryVo("privileges", "40288bb8407b958901407ba3f8f50000,ff8080813c99cf06013c9a434495007b,ff8080813c99cf06013c9a43c86a007c,ff8080813c9a59c9013c9a607c100001,ff8080813c9a59c9013c9a60a32e0002,ff8080813d61d09e013d62c532e50176,ff8080813e4d30a8013e65ed8d8b5c8d")});

		checkinService.checkin(checkinVo);
	}

	@Test
	public void testCheckinUseWebClient() {
		String deviceId = "352123051547838_x", appId = "com.foreveross.push.android.library", alias = "wangyi";
		WebClient client = WebClient.create(TARGET_URL + "/checkinservice", providers);
		client.path("/checkins").accept("application/json").type(MediaType.APPLICATION_JSON).encoding("UTF-8");

		DeviceCheckinVo checkinVo = new DeviceCheckinVo();
		checkinVo.setDeviceId(deviceId);
		checkinVo.setAppId(appId);
		checkinVo.setAlias(alias);
		checkinVo.setChannelId("mina");
		checkinVo.setDeviceName("andriod");
		checkinVo.setGis("128,68");
		checkinVo.setOsName("Android");
		checkinVo.setOsVersion("4.2");
		checkinVo.setPushToken("23");
		checkinVo.setTags(new TagEntryVo[] { new TagEntryVo("location", "gz"), 
				new TagEntryVo("level", "gold"),
				new TagEntryVo("privileges", "40288bb8407b958901407ba3f8f50000,ff8080813c99cf06013c9a434495007b,ff8080813c99cf06013c9a43c86a007c,ff8080813c9a59c9013c9a607c100001,ff8080813c9a59c9013c9a60a32e0002,ff8080813d61d09e013d62c532e50176,ff8080813e4d30a8013e65ed8d8b5c8d")});

		for(int i=0; i<20; i++){
			checkinVo.setDeviceId("352123051547838_"+System.currentTimeMillis()+"_"+i);
			client.post(checkinVo, DeviceCheckinVo.class);
		}
	}
	
	@Test
	public void testUpdateTags(){
		Map<String, String> tags=new HashMap<String, String>();
		tags.put("privileges", "机长,管理员");
		String deviceId = "00000000-4a83-d36b-5dec-90fa07119a81", appId="com.csair.impc";;
		
		this.checkinService.updateTags(deviceId, appId, tags, "admin");
	}

}
