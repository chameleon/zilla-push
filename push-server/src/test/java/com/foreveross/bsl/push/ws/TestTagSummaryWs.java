/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.ws;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.foreveross.bsl.push.application.TagSummaryService;
import com.foreveross.bsl.push.application.vo.TagSummaryVo;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-10-18
 *
 */
public class TestTagSummaryWs extends BasePushWsTest{
	
	private TagSummaryService tagSummaryService;
	
	@Before
	public void setup(){
		tagSummaryService = JAXRSClientFactory.create(this.getTargetUrl(), TagSummaryService.class, providers);
	}
	
	@Test
	public void testUpdateAndRemove(){
		String appId="com.xxx.abc";
		try{
			//测试增加和更新
			tagSummaryService.updateTagSummary(appId, "location", "广州", "深圳");
			TagSummaryVo tagSum=tagSummaryService.getTagSummary(appId);
			Assert.assertNotNull(tagSum);
			Assert.assertEquals(1, tagSum.getTags().size());
			String[] tagValues=tagSum.getTagSet("location").getValues();
			Assert.assertEquals(2, tagValues.length);
			Assert.assertTrue(ArrayUtils.contains(tagValues, "广州"));
			Assert.assertTrue(ArrayUtils.contains(tagValues, "深圳"));
			
			tagSummaryService.updateTagSummary(appId, "level", "gold");
			tagSum=tagSummaryService.getTagSummary(appId);
			Assert.assertEquals(2, tagSum.getTags().size());
			tagValues=tagSum.getTagSet("level").getValues();
			Assert.assertEquals(1, tagValues.length);
			Assert.assertTrue(ArrayUtils.contains(tagValues, "gold"));
			
			tagSummaryService.updateTagSummary(appId, "level", "diamond");
			tagSum=tagSummaryService.getTagSummary(appId);
			tagValues=tagSum.getTagSet("level").getValues();
			Assert.assertEquals(2, tagValues.length);
			Assert.assertTrue(ArrayUtils.contains(tagValues, "gold"));
			Assert.assertTrue(ArrayUtils.contains(tagValues, "diamond"));
			
			//测试删除
			tagSummaryService.removeTag(appId, "level", "diamond");
			tagSum=tagSummaryService.getTagSummary(appId);
			tagValues=tagSum.getTagSet("level").getValues();
			Assert.assertEquals(1, tagValues.length);
			Assert.assertTrue(ArrayUtils.contains(tagValues, "gold"));
			//删除level的diamond
			Assert.assertFalse(ArrayUtils.contains(tagValues, "diamond"));
			tagValues=tagSum.getTagSet("location").getValues();
			Assert.assertTrue(ArrayUtils.contains(tagValues, "广州"));
			Assert.assertTrue(ArrayUtils.contains(tagValues, "深圳"));
			
			//删除level标签
			tagSummaryService.removeTag(appId, "level");
			tagSum=tagSummaryService.getTagSummary(appId);
			Assert.assertNull(tagSum.getTagSet("level"));
			Assert.assertNotNull(tagSum.getTagSet("location"));
			
			//删除location的"广州"，"深圳"
			tagSummaryService.removeTag(appId, "location", "广州", "深圳");
			tagSum=tagSummaryService.getTagSummary(appId);
			Assert.assertNotNull(tagSum.getTagSet("location"));
			Assert.assertEquals(0, tagSum.getTagSet("location").getValues().length);
			
			//删除所有标签
			tagSummaryService.removeAllTags(appId);
			tagSum=tagSummaryService.getTagSummary(appId);
			Assert.assertNull(tagSum.getTags());
			
			tagSummaryService.updateTagSummary(appId, "location", "广州", "深圳", "株洲");
			//删除时使用无效值
			tagSummaryService.removeTag(appId, "location", "XXX", "深圳");
			tagSum=tagSummaryService.getTagSummary(appId);
			Assert.assertNotNull(tagSum.getTagSet("location"));
			tagValues=tagSum.getTagSet("location").getValues();
			Assert.assertEquals(2, tagValues.length);
			Assert.assertTrue(ArrayUtils.contains(tagValues, "广州"));
			Assert.assertTrue(ArrayUtils.contains(tagValues, "株洲"));
		}
		finally{
			tagSummaryService.delete(appId);
		}
	}
	
	@Test
	public void testRemoveTagForAllApp(){
		String appId="com.xxx.abc", appId1="aaa", appId2="bbb";
		try{
			tagSummaryService.updateTagSummary(appId, "role", "机长", "管理员", "空姐");
			tagSummaryService.updateTagSummary(appId1, "role", "机长", "管理员", "空姐");
			tagSummaryService.updateTagSummary(appId2, "role", "机长", "管理员", "空姐");
			
			TagSummaryVo tagSum=tagSummaryService.getTagSummary(appId);
			Assert.assertEquals(3, tagSum.getTagSet("role").getValues().length);
			tagSum=tagSummaryService.getTagSummary(appId1);
			Assert.assertEquals(3, tagSum.getTagSet("role").getValues().length);
			tagSum=tagSummaryService.getTagSummary(appId2);
			Assert.assertEquals(3, tagSum.getTagSet("role").getValues().length);
			
			this.tagSummaryService.removeTagForAllApps("role", "机长");
			tagSum=tagSummaryService.getTagSummary(appId);
			Assert.assertEquals(2, tagSum.getTagSet("role").getValues().length);
			tagSum=tagSummaryService.getTagSummary(appId1);
			Assert.assertEquals(2, tagSum.getTagSet("role").getValues().length);
			tagSum=tagSummaryService.getTagSummary(appId2);
			Assert.assertEquals(2, tagSum.getTagSet("role").getValues().length);
			
			this.tagSummaryService.removeTagForAllApps("role");
			tagSum=tagSummaryService.getTagSummary(appId);
			Assert.assertEquals(0, tagSum.getTags().size());
			tagSum=tagSummaryService.getTagSummary(appId1);
			Assert.assertEquals(0, tagSum.getTags().size());
			tagSum=tagSummaryService.getTagSummary(appId2);
			Assert.assertEquals(0, tagSum.getTags().size());
		}
		finally{
			tagSummaryService.delete(appId);
			tagSummaryService.delete(appId1);
			tagSummaryService.delete(appId2);
		}
	}
}
