package com.foreveross.bsl.push.ws;
/**
 * Copyright (C) 2013-2014 the original author or authors.
 */


/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-8-6
 *
 */
public class BasePushWsTest extends BaseWsTest {
	
	protected final static String TARGET_URL = "http://localhost:18861/push-server/api";

	@Override
	public String getTargetUrl() {
		return TARGET_URL;
	}

}
