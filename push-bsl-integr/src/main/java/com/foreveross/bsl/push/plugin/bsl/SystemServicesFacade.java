/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.plugin.bsl;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.foreveross.bsl.common.utils.context.ContextThead;
import com.foreveross.bsl.push.application.PushModuleException;
import com.foreveross.bsl.system.application.ApplicationRegisterApplication;
import com.foreveross.bsl.system.application.vo.ApplicationRegisterVO;

/**
 * 调用System模块的Web Service接口的门面类
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-8-6
 *
 */
@Named
public class SystemServicesFacade {
	
	private final static Logger log = LoggerFactory.getLogger(SystemServicesFacade.class);

	@Inject
	private ApplicationRegisterApplication appRegService;
	
	private final ApplicationRegisterVO getAppReg(String appKey){
		ApplicationRegisterVO appReg=this.appRegService.getByAppKey(appKey);
		if(appReg==null){
			throw new PushModuleException("不存在appKey为"+appKey+"的应用");
		}
		return appReg;
	}
	
	public final String getAppIdByAppKey(String appKey){
		return getAppReg(appKey).getAppId();
	}
	
	/**
	 * 通过AppKey找到租户用户名，作为数据库路由依据
	 * @param appKey
	 */
	public final void setRepositoryRouterByAppKey(String appKey){
		ApplicationRegisterVO appReg=getAppReg(appKey);
		String username=appReg.getUsername();
		ContextThead.setThreadVar(username);
		log.debug("设置RepositoryRouter的当前线程变量为:{}",username);
	}
}
