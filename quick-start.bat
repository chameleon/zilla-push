@echo off
echo [Pre-Requirement] Makesure install JDK 6.0+ and set the JAVA_HOME.
echo [Pre-Requirement] Makesure install Maven 3.0.3+ and set the PATH.

set MVN=mvn
set MAVEN_OPTS=%MAVEN_OPTS% -XX:MaxPermSize=128m

echo [Step 1] Install all bsl-push modules to local maven repository.
call %MVN% clean install -Dmaven.test.skip=true
if errorlevel 1 goto error

echo [Step 2] Start push-server and push-console.
cd push-server
start "Push-Server" %MVN% clean jetty:run -Djetty.port=18861 
if errorlevel 1 goto error
cd ..\push-console
start "Push-Console" %MVN% clean jetty:run
if errorlevel 1 goto error
cd ..\

echo [INFO] Please wait a moment. When you see "[INFO] Started Jetty Server" in both 2 popup consoles, you can access below demo sites:
echo [INFO] http://localhost:18861/push-server
echo [INFO] http://localhost:8080/push-console

goto end
:error
echo Error Happen!!
:end
pause