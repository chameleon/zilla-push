/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application;

import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.data.domain.Page;

import com.foreveross.bsl.push.application.vo.MessageVo;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-11-8
 *
 */
@Produces(MediaType.APPLICATION_JSON)
@Path("push-msgs")
public interface PushMessageQueryService {

	@GET
	@Path("{appId}/{id}")
	MessageVo get(@PathParam("appId") String appId, @PathParam("id") String id);
	
	/**
	 * 获取已发出但还没有回执的消息
	 * @param token
	 * @param deviceId
	 * @param appId
	 * @return
	 */
	@GET
	@Path("none-receipts/{deviceId}/{appId}")
	List<MessageVo> fetchSentAndNoReceiptMessages(@PathParam("deviceId") String deviceId,
			@PathParam("appId") String appId,
			@QueryParam("page") @DefaultValue("1") Integer pageNumber, 
			@QueryParam("size") @DefaultValue("10") Integer pageSize);

	/**
	 * 获取已发出但还没有回执的消息
	 * @param token
	 * @param deviceId
	 * @param appId
	 * @param pageNumber 页号从1开始
	 * @param pageSize
	 * @return
	 */
	@GET
	@Path("none-receipts/{deviceId}/{appId}/{page}/{size}")
	Page<MessageVo> fetchSentAndNoReceiptMessagePage(@PathParam("deviceId") String deviceId,
			@PathParam("appId") String appId,
			@PathParam("page") Integer pageNumber, 
			@PathParam("size") Integer pageSize);
	
	/**
	 * 使用路径参数方式在某此环境遇到问题（eg:南航测试服务器上）输入的路径为：
	 * http://10.108.68.136:8080/push-server/api/receipts/none-receipts/ffffffff-da82-57d2-ffff-ffffce0af8fe%40csair.cube.openfire.test%2FAMPClient/ffffffff-da82-57d2-ffff-ffffce0af8fe/com.csair.impc
	 * 客户端响应码是400
	 * 所以增加查询参数方式，备用，之前的路径参数方式不删除
	 * @param token
	 * @param deviceId
	 * @param appId
	 * @return
	 */
	@GET
	@Path("none-receipts")
	List<MessageVo> fetchSentAndNoReceiptMessages1(@QueryParam("deviceId") String deviceId,
			@QueryParam("appId") String appId, 
			@QueryParam("page") @DefaultValue("1") Integer pageNumber, 
			@QueryParam("size") @DefaultValue("10") Integer pageSize);
}
