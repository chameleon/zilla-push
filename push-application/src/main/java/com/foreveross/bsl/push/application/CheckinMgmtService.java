package com.foreveross.bsl.push.application;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.data.domain.Page;

import com.foreveross.bsl.common.utils.web.QuerySpecs;
import com.foreveross.bsl.push.application.vo.DeviceCheckinVo;
import com.foreveross.bsl.push.application.vo.MessageParams;

/**
 * 设备签到信息管理接口
 * 
 * @author Wangyi
 * @version v1.0
 * 
 * @date 2013-7-10
 * 
 */
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_JSON })
@Path("checkin-mgmt")
public interface CheckinMgmtService {

	/**
	 * 获取DeviceCheckinVo分页数据,可按VO属性查找
	 * 
	 * @param searchParams
	 *            查找条件，为null或空表示查找所有
	 * @param pageNumber
	 *            第几页，从1开始
	 * @param pageSize
	 *            每页记录数，0表示取默认记录数，默认记录数10
	 * 
	 * @return
	 */
	@GET
	@Path("device-checkins/{pageNumber}/{pageSize}")
	Page<DeviceCheckinVo> findPageBy(@PathParam("pageNumber") @DefaultValue("1") int pageNumber, 
			@PathParam("pageSize") @DefaultValue("10") int pageSize,
			@QueryParam("querySpecs") QuerySpecs querySpecs);

	/**
	 * 删除设备签到信息
	 * 
	 * @param checkinId
	 */
	@DELETE
	@Path("device-checkins/{checkinId}")
	void remove(@PathParam("checkinId") String checkinId);
	
	int clearToken(String token);

	/**
	 * 判断指定消息推送的目标是否存在
	 * 
	 * @param msgParams
	 * @return
	 */
	@GET
	@Path("device-checkins/exists")
	boolean existsTarget(@QueryParam("") MessageParams msgParams);

	/**
	 * 在签到列表中查找指定目标
	 * 
	 * @param msgParams
	 * @param pageable
	 * @return
	 */
	@GET
	@Path("device-checkins/by-msg-params/{pageNumber}/{pageSize}")
	Page<DeviceCheckinVo> findTargets(@PathParam("pageNumber") @DefaultValue("1") int pageNumber, 
			@PathParam("pageSize") @DefaultValue("10") int pageSize,
			@QueryParam("") MessageParams msgParams);

}
