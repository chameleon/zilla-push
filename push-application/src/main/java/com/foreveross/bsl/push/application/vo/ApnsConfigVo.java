package com.foreveross.bsl.push.application.vo;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;

@XmlRootElement(name = "ApnsConfig")
public class ApnsConfigVo {

	private String certFilename;
	private byte[] certContent;
	private String certPassword;
	private Date updateTime;

	public String getCertFilename() {
		return certFilename;
	}

	public void setCertFilename(String certFilename) {
		this.certFilename = certFilename;
	}

	public String getCertPassword() {
		return certPassword;
	}

	public void setCertPassword(String certPassword) {
		this.certPassword = certPassword;
	}

	@JsonIgnore
	public byte[] getCertContent() {
		return certContent;
	}

	public void setCertContent(byte[] certContent) {
		this.certContent = certContent;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

}
