package com.foreveross.bsl.push.application.vo;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;


/**
 * 
 * 设备签到VO,表示一设备上的应用签到时需提交到推送服务器的信息
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-7-9
 *
 */
@XmlRootElement(name = "DeviceCheckin")
public class DeviceCheckinVo {
	/**
	 * 设备签到实体ID, 客户端签到操作时不需要设置
	 */
	private String id;
	
	/**
	 * 设备ID,不能为空
	 */
	private String deviceId;
	
	/**
	 * 应用ID,不能为空
	 */
	private String appId;
	
	/**
	 * 推送Token,不能为空；推送Token是指终端应用到APNS或Mina服务器注册时拿到的token
	 */
	private String pushToken;
	
	/**
	 * 推送渠道ID, 不能为空,必须是推送服务器指定的ID,目前有:apns,apns_sandbox,mina,openfire
	 */
	private String channelId;
	
	/**
	 * 设备名称
	 */
	private String deviceName;
	
	/**
	 * 设备操作系统名称
	 */
	private String osName;
	
	/**
	 * 设备操作系统版本
	 */
	private String osVersion;
	
	/**
	 * 设备所处地理位置信息
	 */
	private String gis;
	
	/**
	 * 别名,一般来说就是指用户登录名
	 */
	private String alias;
	
	/**
	 * 最近签到时间
	 */
	private Date checkinTime;
	
	/**
	 * 标签数组,标签使用key-value表示，用于标签当前应用或用户的某些属性
	 */
	private TagEntryVo[] tags;

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getPushToken() {
		return pushToken;
	}

	public void setPushToken(String pushToken) {
		this.pushToken = pushToken;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getOsName() {
		return osName;
	}

	public void setOsName(String osName) {
		this.osName = osName;
	}

	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	public String getGis() {
		return gis;
	}

	public void setGis(String gis) {
		this.gis = gis;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public TagEntryVo[] getTags() {
		return tags;
	}

	public void setTags(TagEntryVo[] tags) {
		this.tags = tags;
	}

	@JsonIgnore 
	public boolean isIOSDevice(){
		if(this.osName!=null){
			String str=this.osName.toLowerCase();
			return str.contains("ios");
		}
		return false;
	}
	
	@JsonIgnore
	public List<TagSummaryVo.TagSet> getCategoryTags(){
		List<TagSummaryVo.TagSet> tagSet=Lists.newLinkedList();
		if(this.tags==null || this.tags.length==0){
			return tagSet;
		}
		Map<String, List<String>> map=Maps.newHashMap();
		List<String> values=null;
		for(TagEntryVo t : tags){
			values=map.get(t.getKey());
			if(values==null){
				values=Lists.newArrayList();
				map.put(t.getKey(), values);
			}
			values.add(t.getValue());
		}
		for(Map.Entry<String, List<String>> entry : map.entrySet()){
			tagSet.add(new TagSummaryVo.TagSet(entry.getKey(), entry.getValue().toArray(new String[0])));
		}
		return tagSet;
	}

	/**
	 * @return the checkinTime
	 */
	public Date getCheckinTime() {
		return checkinTime;
	}

	/**
	 * @param checkinTime the checkinTime to set
	 */
	public void setCheckinTime(Date checkinTime) {
		this.checkinTime = checkinTime;
	}

}
