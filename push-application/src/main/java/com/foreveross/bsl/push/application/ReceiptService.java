/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

/**
 * 推送回执接口
 * 
 * @author Wangyi
 * @version v1.0
 * 
 * @date 2013-8-6
 * 
 */
@Path("receipts")
public interface ReceiptService {

	/**
	 * 推送回执
	 * 
	 * @param deviceId
	 *            设备ID,不能为空
	 * @param sendId
	 *            发送ID,不能为空;sendId是在发送消息时会生成的，与消息一起发送到终端应用；可用半角逗号","连接多个sendId
	 * @param appId
	 *            不能为空，appKey为应用打包时系统生成的唯一标识
	 */
	@PUT
	@Path("")
	@Consumes({ MediaType.APPLICATION_FORM_URLENCODED })
	void receipt(@FormParam("deviceId") String deviceId, @FormParam("msgId") String msgId, @FormParam("appId") String appId);

}
