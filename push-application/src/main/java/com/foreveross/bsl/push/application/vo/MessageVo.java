package com.foreveross.bsl.push.application.vo;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.StringUtils;

import com.foreveross.bsl.push.application.MessageException;
import com.google.common.collect.Maps;

/**
 * 消息表示类
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-7-12
 *
 */
@XmlRootElement(name = "Message")
public class MessageVo {
	
	public static final int MAX_TITLE_BYTE_LEN=154;
	public static final int MAX_CONTENT_LEN=1000;
	
	public enum MessageTypeEnum{
		/**
		 * 系统消息，即不限定终端应用上的某个模块，只在“消息盒子”里查看
		 */
		SYS("系统消息"),
		/**
		 * 模块消息，针对某个特定的模块，模块图标上会显示未读消息条数，“消息盒子”仅看到标题
		 */
		MODULE("模块消息"), 
		/**
		 * 设备控制消息，在终端不作显示，用于处理控制设备
		 */
		MDM("MDM消息"),
		/**
		 * 安全策略变更消息
		 */
		SECURITY("安全策略变更消息");
		
		
		private final String description;
		
		MessageTypeEnum(String description){
			this.description=description;
		}

		/**
		 * @return the description
		 */
		public String getDescription() {
			return description;
		}
		
	}
	
	private String id;
	
	/**
	 * 消息标题
	 */
	private String title;
	
	/**
	 * 消息内容
	 */
	private String content;
	
	/**
	 * 消息类型
	 */
	private MessageTypeEnum messageType = MessageTypeEnum.SYS;
	
	/**
	 * 更多的附属信息，客户端根据需求添加自己的数据
	 */
	private final Map<String, Object> extras = new HashMap<String, Object>();
	
	/**
	 * 需要直接发送到推送渠道的extras属性，默认是不发送任何extras属性
	 */
	private String[] directExtrasPropertys;

	public MessageVo() {
	}

	public MessageVo(String title, String content) {
		super();
		this.title = title;
		this.content = content;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Map<String, Object> getExtras() {
		return Maps.newHashMap(extras);
	}

	public void setExtras(Map<String, Object> extras) {
		this.extras.putAll(extras);
	}
	
	public MessageVo putExtra(String key, Object obj){
		this.extras.put(key, obj);
		return this;
	}
	
	public MessageVo removeExtra(String key){
		this.extras.remove(key);
		return this;
	}
	
	public Object getExtra(String key){
		return this.extras.get(key);
	}
	
	public boolean containsExtra(String key){
		return this.extras.containsKey(key);
	}
	
	public void clearExtra(){
		this.extras.clear();
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	/**
	 * @return the messageType
	 */
	public MessageTypeEnum getMessageType() {
		return messageType;
	}

	/**
	 * @param messageType the messageType to set
	 */
	public void setMessageType(MessageTypeEnum messageType) {
		this.messageType = messageType;
	}

	public void validate(){
		StringBuilder sb=new StringBuilder();
		String title=this.getTitle();
		if(title!=null && title.getBytes().length>MAX_TITLE_BYTE_LEN){
			sb.append("消息的标题长度不能超过").append(MAX_TITLE_BYTE_LEN).append("字节,1个汉字3个字节(utf-8编码 )");
			throw new MessageException(sb.toString());
		}
		String content=this.getContent();
		if(content!=null && content.length()>MAX_CONTENT_LEN){
			sb.append("消息的正文长度不能超过").append(MAX_CONTENT_LEN).append("个字符");
			throw new MessageException(sb.toString());
		}
		if(StringUtils.isEmpty(title) && StringUtils.isEmpty(content) && extras.isEmpty()){
			throw new MessageException("提交的消息标题、正文或自定义属性不能都为空");
		}
	}

	/**
	 * @return the directExtrasPropertys
	 */
	public String[] getDirectExtrasPropertys() {
		return directExtrasPropertys;
	}

	/**
	 * @param directExtrasPropertys the directExtrasPropertys to set
	 */
	public void setDirectExtrasPropertys(String[] directExtrasPropertys) {
		this.directExtrasPropertys = directExtrasPropertys;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

}
