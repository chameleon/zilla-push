/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-9-5
 *
 */
public interface DsRouterIdentifiable {
	void setDsRouter(String dsRouter);
	String getDsRouter();
}
