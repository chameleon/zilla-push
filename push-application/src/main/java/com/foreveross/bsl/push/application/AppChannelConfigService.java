package com.foreveross.bsl.push.application;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.cxf.jaxrs.ext.multipart.Multipart;
import org.springframework.data.domain.Page;

import com.foreveross.bsl.common.utils.web.QuerySpecs;
import com.foreveross.bsl.push.application.vo.AppChannelConfigVo;

/**
 * 应用相对于各推送渠道用于推送的配置接口
 * 
 * @author Wangyi
 * 
 */
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_JSON })
@Path("appchannel-config")
public interface AppChannelConfigService {
    
	@GET
	@Path("appchannelcfgs/exists/{appId}")
	boolean existsConfig(@PathParam("appId") String appId);

	@GET
	@Path("appchannelcfgs/{appId}")
	AppChannelConfigVo getConfig(@PathParam("appId") String appId);

	@GET
	@Path("appchannelcfgs")
	List<AppChannelConfigVo> getConfigs(@QueryParam("appIds") Set<String> appIds);

	@GET
	@Path("appchannelcfgs/{pageNo}/{pageSize}")
	Page<AppChannelConfigVo> findBy(@PathParam("pageNo") int pageNo,
			@PathParam("pageSize") int pageSize, @QueryParam("q") QuerySpecs querySpecs);

	@DELETE
	@Path("appchannelcfgs/{appId}")
	void removeConfig(@PathParam("appId") String appId);

	@POST
	@Path("appchannelcfgs/apns/{appId}")
	@Consumes("multipart/mixed")
	void setApnsConfig(@PathParam("appId") String appId, 
			@Multipart("certInputStream") InputStream certInputStream, 
			@Multipart("certFilename") String certFilename, 
			@Multipart("certPassword") String certPassword, 
			@Multipart("sandbox") boolean sandbox)
			throws IOException;
	
	@PUT
	@Path("appchannelcfgs/apns/{appId}")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	void updateApnsConfig(@PathParam("appId")String appId, 
			@FormParam("certPassword") String certPassword,
			@FormParam("sandbox") boolean sandbox);

	@PUT
	@Path("appchannelcfgs/openfire/{appId}")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	void setOpenfireConfig(@PathParam("appId")String appId, @FormParam("username") String username, 
			@FormParam("password") String password);
	
	@PUT
	@Path("appchannelcfgs/instant/{appId}")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	void setInstantConfig(@PathParam("appId")String appId);
}