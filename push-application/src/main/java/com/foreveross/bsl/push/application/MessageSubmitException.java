/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-9-2
 *
 */
public class MessageSubmitException extends MessageException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6196284382882329642L;

	public MessageSubmitException(String message, Throwable cause) {
		super(message, cause);
	}

	public MessageSubmitException(String message) {
		super(message);
	}

	public MessageSubmitException(Throwable cause) {
		super(cause);
	}
	
}
