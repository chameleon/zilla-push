package com.foreveross.bsl.push.application.vo;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "InstantConfig")
public class InstantConfigVo {
	
	private boolean checked;

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}
}
