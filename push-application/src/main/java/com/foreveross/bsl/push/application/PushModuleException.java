package com.foreveross.bsl.push.application;


public class PushModuleException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 814244765747752129L;

	public PushModuleException() {
		super();
	}

	public PushModuleException(String message, Throwable cause) {
		super(message, cause);
	}

	public PushModuleException(String message) {
		super(message);
	}

	public PushModuleException(Throwable cause) {
		super(cause);
	}

}
