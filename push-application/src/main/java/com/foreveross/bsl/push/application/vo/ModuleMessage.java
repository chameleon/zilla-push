/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application.vo;

import java.util.HashSet;
import java.util.Set;

/**
 * 模块消息类
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-8-2
 *
 */
public class ModuleMessage extends MessageVo {

	public static final String KEY_MODULE_ID="moduleIdentifer";
	public static final String KEY_MODULE_NAME="moduleName";
	
	public ModuleMessage() {
		super();
		setDirectExtrasPropertys(null);
	}

	public ModuleMessage(String title, String content) {
		super(title, content);
		setDirectExtrasPropertys(null);
	}
	
	@Override
	public void setDirectExtrasPropertys(String[] directExtrasPropertys) {
		Set<String> set=new HashSet<String>();
		set.add(KEY_MODULE_ID);
		set.add(KEY_MODULE_NAME);
		if(directExtrasPropertys!=null && directExtrasPropertys.length>0){
			for(String p : directExtrasPropertys){
				set.add(p);
			}
		}
		super.setDirectExtrasPropertys(set.toArray(new String[0]));
	}

	@Override
	public MessageTypeEnum getMessageType() {
		return MessageTypeEnum.MODULE;
	}
	
	@Override
	public void setMessageType(MessageTypeEnum messageType) {

	}
	
	/**
	 * 
	 * @return
	 */
	public String getModuleIdentifer(){
		return (String)this.getExtra(KEY_MODULE_ID);
	}
	
	/**
	 * 设置模块的identifer, 此identifer并非模块实体的标识，而是业务代号，如公告模块的identifer为：com.foss.announcement
	 * 模块
	 * @param moduleId
	 */
	public void setModuleIdentifer(String moduleId){
		this.putExtra(KEY_MODULE_ID, moduleId);
	}
	
	public String getModuleName(){
		return (String)this.getExtra(KEY_MODULE_NAME);
	}
	
	public void setModuleName(String moduleName){
		this.putExtra(KEY_MODULE_NAME, moduleName);
	}
}
