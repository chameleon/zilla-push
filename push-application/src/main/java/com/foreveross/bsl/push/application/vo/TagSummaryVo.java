/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application.vo;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 租户应用所有标签摘要VO, 一个键对应多个标签值
 * @author Wangyi
 * @version v1.0
 * 
 * @date 2013-7-15
 * 
 */
@XmlRootElement(name = "TagSummary")
public class TagSummaryVo {
	
	private String appId;
	private Date updateTime;
	private List<TagSet> tags;
	
	/**
	 * 关于标签键的标签的集合
	 *
	 * @author Wangyi
	 * @version v1.0
	 *
	 * @date 2013-8-31
	 *
	 */
	public static class TagSet {
		private String key;
		private String[] values;
		
		/**
		 * 
		 */
		public TagSet() {
		}
		
		public TagSet(String key, String[] values) {
			this.key=key;
			this.values=Arrays.copyOf(values, values.length);
		}

		/**
		 * @return the key
		 */
		public String getKey() {
			return key;
		}

		/**
		 * @param key
		 *            the key to set
		 */
		public void setKey(String key) {
			this.key = key;
		}

		/**
		 * @return the values
		 */
		public String[] getValues() {
			return values;
		}

		/**
		 * @param values
		 *            the values to set
		 */
		public void setValues(String[] values) {
			this.values = values;
		}

	}
	
	public TagSet getTagSet(String key){
		for(TagSet tag : tags){
			if(tag.getKey().equals(key)){
				return tag;
			}
		}
		return null;
	}

	/**
	 * @return the appId
	 */
	public String getAppId() {
		return appId;
	}

	/**
	 * @param appId
	 *            the appId to set
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}

	/**
	 * @return the updateTime
	 */
	public Date getUpdateTime() {
		return updateTime;
	}

	/**
	 * @param updateTime the updateTime to set
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return the tags
	 */
	public List<TagSet> getTags() {
		return tags;
	}

	/**
	 * @param tags the tags to set
	 */
	public void setTags(List<TagSet> tags) {
		this.tags = tags;
	}

}
