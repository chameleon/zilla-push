/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.foreveross.bsl.push.application.vo.TagSummaryVo;

/**
 * 标签摘要服务接口
 * 
 * @author Wangyi
 * @version v1.0
 * 
 * @date 2013-7-15
 * 
 */
@Produces(MediaType.APPLICATION_JSON)
@Path("tagservice")
public interface TagSummaryService {

	/**
	 * 更新应用的标签摘要，如果标签摘要不存在则新增
	 * 
	 * @param appId
	 * @param key
	 * @param tagValues
	 */
	@PUT
	@Path("tagsummary/{appId}")
	void updateTagSummary(@PathParam("appId") String appId, @QueryParam("key") String key,
			@QueryParam("values") String... tagValues);

	/**
	 * 按应用ID查找该应用的设备所有打过的标签
	 * 
	 * @param appId
	 * @return
	 */
	@GET
	@Path("tagsummary/{appId}")
	TagSummaryVo getTagSummary(@PathParam("appId") String appId);

	/**
	 * 删除指定应用的标签摘要实体
	 * 
	 * @param appId
	 */
	@DELETE
	@Path("tagsummary/{appId}")
	void delete(@PathParam("appId") String appId);

	/**
	 * 获取应用的某个标签键的所有值列表
	 * 
	 * @param appId
	 * @param key
	 * @return
	 */
	@GET
	@Path("tags/{appId}/{key}")
	String[] getTagValues(@PathParam("appId") String appId, @PathParam("key") String key);

	/**
	 * 删除指定应用ID的标签键的标签值,标签值可以指定多个
	 * 
	 * @param appId
	 * @param key
	 * @param values
	 */
	@DELETE
	@Path("tags/v/{appId}/{key}")
	void removeTag(@PathParam("appId") String appId, @PathParam("key") String key, @QueryParam("t") String... values);

	/**
	 * 删除指定应用ID的指定标签,包括键和所有值
	 * 
	 * @param appId
	 * @param key
	 */
	@DELETE
	@Path("tags/{appId}}/{key}")
	void removeTag(@PathParam("appId") String appId, @PathParam("key") String key);

	/**
	 * 删除指定应用的所有标签
	 * 
	 * @param appId
	 */
	@DELETE
	@Path("tags/{appId}}")
	void removeAllTags(@PathParam("appId") String appId);
	
	/**
	 * 删除所有应用的标签键的标签值,标签值可以指定多个
	 * 
	 * @param appId
	 * @param key
	 * @param values
	 */
	@DELETE
	@Path("tags-all-app/v/{key}")
	void removeTagForAllApps(@PathParam("key") String key, @QueryParam("t") String... values);

	/**
	 * 删除所有应用的指定标签,包括键和所有值
	 * 
	 * @param appId
	 * @param key
	 */
	@DELETE
	@Path("tags-all-app/{key}")
	void removeTagForAllApps(@PathParam("key") String key);
}
