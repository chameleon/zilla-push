/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application.impl;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-11-21
 *
 */
public class TestOpenfireClientApi {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Test
	public void testSendMessage() throws Exception {
		String token="ffffffff-da82-57d2-743c-41910033c587@172.16.65.11/SOC_Client";
//		String token="00000000-336b-83e4-6d3c-628e0033c587@cube-server";
		Connection conn=buildOpenfireConnection();
		final CountDownLatch latch=new CountDownLatch(1);
		Chat newChat = conn.getChatManager().createChat(token, new MessageListener() {
			@Override
			public void processMessage(Chat paramChat, Message paramMsg) {
				log.debug("received message: {}", paramMsg.toXML());
				log.debug("processMessage() ThreadID:{}, Thread:{}", paramChat.getThreadID(), paramMsg.getThread());
				latch.countDown();
			}
		});
		String msgJson="\"message\":{\"title\":\"API TEST\",\"content\":\"bbbb!!!\",\"messageType\":\"SYS\"}";
		log.debug("message: {}", msgJson);
		newChat.sendMessage(msgJson);
		log.debug("message send");
		latch.await(15, TimeUnit.SECONDS);
		log.debug("finish!");
	}

	private Connection buildOpenfireConnection() throws XMPPException{
		String host="172.16.65.11", username="com.csair.soc", password="123456";
//		String host="10.108.68.136", username="com.csair.amp", password="123456";
		int port=5222;
		
		log.debug("instance connection...");
		// Create the configuration for this new connection
		ConnectionConfiguration config = new ConnectionConfiguration(host, port);
		config.setCompressionEnabled(true);
		config.setSASLAuthenticationEnabled(true);
		Connection conn = new XMPPConnection(config);
		if (!conn.isConnected()){ 
			log.debug("begin connect to Openfire server {}:{}...", new Object[]{host, port});
			conn.connect();
			log.debug("connected: {}", conn.isConnected());
		}
		if(!conn.isAuthenticated()) {
			log.debug("begin login to server by user {}:{}...", username, password);
			conn.login(username, password);
			log.debug("authenticated: {}", conn.isAuthenticated());
			log.debug("Openfire的连接已准备好!");
		}
		return conn;
	}
	
}
