package com.foreveross.bsl.push.application.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsDelegate;
import com.notnoop.apns.ApnsNotification;
import com.notnoop.apns.ApnsService;
import com.notnoop.apns.ApnsServiceBuilder;
import com.notnoop.apns.DeliveryError;

public class TestApnsClientApi {

	private final static Logger log = LoggerFactory.getLogger(TestApnsClientApi.class);

	private void doSend(String token, String certfile, String certPassword, String msg, boolean sandbox)
			throws IOException {
		this.doSend(new String[]{token}, certfile, certPassword, msg, sandbox, 1);
	}
	
	private void doSend(String token, String certfile, String certPassword, String msg, 
			boolean sandbox, int repeat) throws IOException {
		this.doSend(new String[]{token}, certfile, certPassword, msg, sandbox, repeat);
	}
	
	private void doSend(String[] tokens, String certfile, String certPassword, String msg, 
			boolean sandbox)
			throws IOException {
		this.doSend(tokens, certfile, certPassword, msg, sandbox, 1);
	}
	
	private void doSend(String[] tokens, String certfile, String certPassword, String msg, 
			boolean sandbox, int repeat)
			throws IOException {
		File certFile = new File(System.getProperty("user.dir") + "/src/test/resources/apns/" + certfile);
		log.info("cert file:{} exist? {}", certFile, certFile.exists());
		InputStream certIs = new FileInputStream(certFile);
		try {
			ApnsServiceBuilder asb = APNS.newService().withCert(certIs, certPassword).withDelegate(new ApnsLogDelegate());
			if (sandbox) {
				asb.withSandboxDestination();
			} else {
				asb.withProductionDestination();
			}
			//asb.asPool(1);
			ApnsService apns = asb.build();
			for(int i=0; i<repeat; i++){
				Collection<String> tokenList=Arrays.asList(tokens);
				apns.push(tokenList, getDefaultPayload(msg+" "+(i+1)));
				if(repeat>1){
					try {
						Thread.sleep(300);
					} catch (InterruptedException e) {
					}
				}
			}
		} finally {
			if (certIs != null) {
				certIs.close();
			}
		}
	}

	private String getDefaultPayload(String msg) {
		SimpleDateFormat sdf=new SimpleDateFormat("MM-dd HH:mm:ss");
		msg=msg+" "+sdf.format(new Date());
		return APNS.newPayload().alertBody(msg).badge(1).sound("default").build();
	}

	@Test
	public void testPushToImpc() throws Exception {
		String token = "CC34F95D9C7D066574F463C9B0EB1261F329EF93CBD908D224B8253411FCB3BC";
		String certfile = "impc_push_dist.p12";
		String certPassword = "123456";
		String msg = "Hello Test!";
		this.doSend(token, certfile, certPassword, msg, false);
		Thread.sleep(2000);
	}
	
	@Test
	public void testPushToImpcMulti() throws Exception {
		String tokens[] = {
				//"CC34F95D9C7D066574F463C9B0EB1261F329EF93CBD908D224B8253411FCB3BC",
				"E248C89B4994C68D8C9EE0C03B22A716A6394CDE8E2DCF37C647092B937B7A88",
				"A852C05FBC05265DCC772B06AFFAA4EA4934FB3A042C1010FE5FFF3773B87E98",
				"234CA636F14A7A422B926B3170B0C735ACE24522EF3CE861247A3015E64403A3",
				"729853C31DC7E76CC393CBDEF007A5E53C877B537F49F956CEB19F4AAE9438BC",
				"09E9FC19650DF345269F4028F902C3CED78A5094C2A39F62890F85DC5623F54C",
			};
		String certfile = "impc_push_dist.p12";
		String certPassword = "123456";
		String msg = "Hello Test!";
		this.doSend(tokens, certfile, certPassword, msg, false, 10);
		Thread.sleep(8000);
	}

	@Test
	public void testPushToImpcSandbox() throws Exception {
		String token = "CC34F95D9C7D066574F463C9B0EB1261F329EF93CBD908D224B8253411FCB3BC";
		String certfile = "impc_push_sandbox.p12";
		String certPassword = "123456";
		String msg = "Hello Test!";
		this.doSend(token, certfile, certPassword, msg, true);
		Thread.sleep(2000);
	}

	@Test
	public void testPushToChameleon() throws Exception {
		String tokens[] = {
				"E248C89B4994C68D8C9EE0C03B22A716A6394CDE8E2DCF37C647092B937B7A88",
				"09E9FC19650DF345269F4028F902C3CED78A5094C2A39F62890F85DC5623F54"
			};
		String certfile = "dis_bsl2.p12";
		String certPassword = "123456";
		// 256-102=154
		String msg = "你好，变色龙！!";
		this.doSend(tokens, certfile, certPassword, msg, false, 1);
		Thread.sleep(60000);
	}

	@Test
	public void testPushToChameleonDev() throws Exception {
		String token = "6627B94CF4C9B05705B848C93B5A7E3487F0089E794E9056ABB275AC1353EFD4";
		String certfile = "dev_bsl2.p12";
		String certPassword = "123456";
		String msg = "Hello Test!";
		this.doSend(token, certfile, certPassword, msg, true, 10);
		Thread.sleep(2000);
	}
	
	@Test
	public void testPushToKecang() throws Exception {
		String token = "CA618957460D19769F37084DAE9C2F82BCE8AE127EDA0C0125DC793A56FCDA32";
		String certfile = "Development_Push_csmobile.p12";
		String certPassword = "csmobile095539";
		String msg = "你好，客舱!";
		this.doSend(token, certfile, certPassword, msg, true, 10);
		Thread.sleep(2000);
	}
	
	@Test
	public void testPushToSoc() throws Exception {
		String token = "E248C89B4994C68D8C9EE0C03B22A716A6394CDE8E2DCF37C647092B937B7A88";
		String certfile = "dis_soc.p12";
		String certPassword = "123456";
		String msg = "你好，运行网!";
		this.doSend(token, certfile, certPassword, msg, false, 5);
		Thread.sleep(2000);
	}
	
	@Test
	public void testPushToSoc1() throws Exception {
		String token = "B5FC4AA99D6456D0F86FC790EC70D5EB304A57D529BE33905F1A68EB94745897";
		String certfile = "dis_soc.p12";
		String certPassword = "123456";
		String msg = "你好，运行网!";
		this.doSend(token, certfile, certPassword, msg, false, 5);
		Thread.sleep(2000);
	}

	@Test
	public void testStringSize() throws UnsupportedEncodingException {
		String title = "abcefsfds*abcefsfds*abcefsfds*abcefsfds*abcefsfds*abcefsfds*abcefsfds*abcefsfds*abcefsfds*abcefsfds*abcefsfds*abcefsfds*abcefsfds*abcefsfds*abcefsfds*abce";
		String title1 = "地要地有人要地人我地地要地有人要地人我地地要地有人要地人我地地要地有人要地人我地地要地有人要地人我地地要地有人要地人我地地要地有人要地人我地地要地有人要地";
		Assert.assertEquals(154, title.length());
		Assert.assertEquals(77, title1.length());
		Assert.assertEquals(154, title1.getBytes("gbk").length);
		Assert.assertEquals(231, title1.getBytes("utf-8").length);
		Assert.assertEquals(231, title1.getBytes().length);
	}
	
	static class ApnsLogDelegate implements ApnsDelegate {

		private static final Logger log = LoggerFactory.getLogger(ApnsLogDelegate.class);

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.notnoop.apns.ApnsDelegate#messageSent(com.notnoop.apns.ApnsNotification
		 * , boolean)
		 */
		@Override
		public void messageSent(ApnsNotification message, boolean resent) {
			log.debug("messageSent() msg: {}, resent: {}", message, resent);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.notnoop.apns.ApnsDelegate#messageSendFailed(com.notnoop.apns.
		 * ApnsNotification, java.lang.Throwable)
		 */
		@Override
		public void messageSendFailed(ApnsNotification message, Throwable e) {
			log.error("messageSendFailed() msg: " + message, e);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.notnoop.apns.ApnsDelegate#connectionClosed(com.notnoop.apns.DeliveryError
		 * , int)
		 */
		@Override
		public void connectionClosed(DeliveryError e, int messageIdentifier) {
			log.error("connectionClosed() e:{}, msgId:{}", e, messageIdentifier);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.notnoop.apns.ApnsDelegate#cacheLengthExceeded(int)
		 */
		@Override
		public void cacheLengthExceeded(int newCacheLength) {
			log.debug("cacheLengthExceeded() new cache length:{}", newCacheLength);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.notnoop.apns.ApnsDelegate#notificationsResent(int)
		 */
		@Override
		public void notificationsResent(int resendCount) {
			log.debug("notificationsResent() resendCount:{}", resendCount);
		}

	}
}
