/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application.impl.channel.apns;

import com.foreveross.bsl.push.domain.IOSExtra;
import com.foreveross.bsl.push.domain.Message;
import com.notnoop.apns.APNS;
import com.notnoop.apns.PayloadBuilder;

/**
 * xxx
 * 
 * @author Wangyi
 * @version v1.0
 * 
 * @date 2013-8-8
 * 
 */
public class ApnMessageConverter {

	/**
	 * 
	 * @param msg
	 * @return
	 */
	public final static String convert2ApnStyleMessage(Message msg) {
		// APNS限制其Payload的大小为256个字节
		PayloadBuilder pb = APNS.newPayload().alertBody(msg.getTitle());
		if (msg.containsExtra(IOSExtra.IOS_EXTRA_KEY)) {
			IOSExtra iosExtra = (IOSExtra) msg.getExtra(IOSExtra.IOS_EXTRA_KEY);
			if (iosExtra != null) {
				pb.badge(iosExtra.getBadge()).sound(iosExtra.getSound());
			}
		} else {
			pb.badge(0).sound("default");
		}
		// 消息的其他属性在客户端收到通知后主动拉取。
		pb.customField("id", msg.getId());
		// 处理指定需要推过去的extras中的属性
		String[] deps = msg.getDirectExtrasPropertys();
		if (deps != null && deps.length > 0) {
			for (String key : deps) {
				Object value = msg.getExtra(key);
				if (value != null) {
					pb.customField(key, value);
				}
			}
		}
		return pb.build();
	}

}
