/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application.impl;

import java.util.concurrent.Executors;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;

import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;

/**
 * EventBusFactoryBean
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-7-10
 *
 */
@Component
public class EventBusFactoryBean implements FactoryBean<EventBus> {
	
	private boolean useAsync=true;
	
	private String busName="defaultEventBus";
	
	private int threadPoolSize=5;

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.FactoryBean#getObject()
	 */
	@Override
	public EventBus getObject() throws Exception {
		if(useAsync){
			//TODO 构建只有一个阻塞队列，如：ArrayBlockingQueue的线程池，实现消息的顺序执行
			return new AsyncEventBus(busName, Executors.newFixedThreadPool(threadPoolSize));
		}
		else{
			return new EventBus(busName);
		}
	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.FactoryBean#getObjectType()
	 */
	@Override
	public Class<?> getObjectType() {
		return EventBus.class;
	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.FactoryBean#isSingleton()
	 */
	@Override
	public boolean isSingleton() {
		return true;
	}

	public boolean isUseAsync() {
		return useAsync;
	}

	public void setUseAsync(boolean useAsync) {
		this.useAsync = useAsync;
	}

	public String getBusName() {
		return busName;
	}

	public void setBusName(String busName) {
		this.busName = busName;
	}

	public int getThreadPoolSize() {
		return threadPoolSize;
	}

	public void setThreadPoolSize(int threadPoolSize) {
		this.threadPoolSize = threadPoolSize;
	}

}
