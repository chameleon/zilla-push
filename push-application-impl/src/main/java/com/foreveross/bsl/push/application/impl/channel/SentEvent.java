package com.foreveross.bsl.push.application.impl.channel;

import java.util.Collection;
import java.util.Date;

import com.foreveross.bsl.push.application.Event;

public class SentEvent extends Event {
	
	private final Date sentTime;
	private final Collection<String> pushIds;
	private final boolean success;
	private final String error;
	private final Exception exception;
	
	public static SentEvent success(Date sentTime, Collection<String> pushIds){
		return new SentEvent(sentTime, pushIds, true, null, null);
	}
	
	public static SentEvent error(Date sendTime, Collection<String> pushIds, String error, Exception exception){
		return new SentEvent(sendTime, pushIds, false, error, exception);
	}
	
	public static SentEvent error(Date sentTime, Collection<String> pushIds, String error){
		return new SentEvent(sentTime, pushIds, false, error, null);
	}
	
	private SentEvent(Date sentTime, Collection<String> pushIds, boolean success, String error, Exception exception) {
		super();
		this.sentTime = sentTime;
		this.pushIds = pushIds;
		this.success = success;
		this.error = error;
		this.exception = exception;
	}

	public Date getSentTime() {
		return sentTime;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getError() {
		return error;
	}

	public Exception getException() {
		return exception;
	}

	public Collection<String> getPushIds() {
		return pushIds;
	}
	
}
