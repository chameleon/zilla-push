package com.foreveross.bsl.push.application.impl.channel;

import java.util.Collection;
import java.util.Date;

import com.foreveross.bsl.push.application.Event;

public class SendingEvent extends Event {

	private final Date sendingTime;
	private final Collection<String> pushIds;

	public SendingEvent(Date sendingTime, Collection<String> pushIds) {
		super();
		this.sendingTime = sendingTime;
		this.pushIds = pushIds;
	}

	public Date getSendingTime() {
		return sendingTime;
	}

	public Collection<String> getPushIds() {
		return pushIds;
	}

}
