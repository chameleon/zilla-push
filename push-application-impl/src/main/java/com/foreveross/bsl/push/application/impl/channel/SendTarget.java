package com.foreveross.bsl.push.application.impl.channel;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class SendTarget {

	private Set<TargetIdentity> targets;
	private String appId;
	private String channelId;

	public static class TargetIdentity {

		private String token;
		private String deviceId;
		private String pushId;
		
		public TargetIdentity() {
		}
		
		public TargetIdentity(String token, String deviceId, String pushId) {
			super();
			this.token = token;
			this.deviceId = deviceId;
			this.pushId = pushId;
		}

		public String getToken() {
			return token;
		}

		public void setToken(String token) {
			this.token = token;
		}

		public String getDeviceId() {
			return deviceId;
		}

		public void setDeviceId(String deviceId) {
			this.deviceId = deviceId;
		}

		public String getPushId() {
			return pushId;
		}

		public void setPushId(String pushId) {
			this.pushId = pushId;
		}
		
		@Override
		public boolean equals(Object obj) {
			return EqualsBuilder.reflectionEquals(this, obj);
		}
		
		@Override
		public int hashCode() {
			return HashCodeBuilder.reflectionHashCode(this);
		}

	}

	public Set<TargetIdentity> getTargets() {
		if(targets==null){
			targets=Sets.newHashSet();
		}
		return targets;
	}

	public void setTargets(Set<TargetIdentity> targets) {
		this.targets = targets;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}
	
	public final Collection<String> getTokens(){
		Set<String> tokens=Sets.newHashSet();
		for(TargetIdentity it : this.targets){
			tokens.add(it.getToken());
		}
		return tokens;
	}
	
	public final Collection<String> getPushIds(){
		Set<String> pushIds=Sets.newHashSet();
		for(TargetIdentity it : this.targets){
			pushIds.add(it.getPushId());
		}
		return pushIds;
	}
	
	public final Collection<String> getDeviceIds(){
		List<String> deviceIds=Lists.newArrayList();
		for(TargetIdentity it : this.targets){
			deviceIds.add(it.getDeviceId());
		}
		return deviceIds;
	}

	/**
	 * @return the channelId
	 */
	public String getChannelId() {
		return channelId;
	}

	/**
	 * @param channelId the channelId to set
	 */
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
}
