package com.foreveross.bsl.push.application.impl;

import com.foreveross.bsl.push.application.vo.MessageParams;
import com.foreveross.bsl.push.domain.entity.Push;
import com.foreveross.bsl.push.domain.entity.PushRequest;

public interface PushDispatcher {
	PushRequest dispatch(MessageParams msgParams);

	@Deprecated
	void dispatch(Push push);
}
