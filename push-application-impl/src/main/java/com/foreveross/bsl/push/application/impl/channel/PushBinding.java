/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application.impl.channel;

import java.util.Map;

import com.foreveross.bsl.push.application.impl.DsRouterPushId;
import com.google.common.collect.Maps;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-9-4
 *
 */
public class PushBinding {
	
	private final Map<String, DsRouterPushId> msg2Pushs=Maps.newConcurrentMap();
	
	public PushBinding put(String msgId, String pushId, String dsRouter){
		msg2Pushs.put(msgId, new DsRouterPushId(pushId, dsRouter));
		return this;
	}
	
	public PushBinding put(String msgId, DsRouterPushId pair){
		msg2Pushs.put(msgId, pair);
		return this;
	}
	
	public DsRouterPushId pop(String msgId){
		//TODO fixme 由于Java-apns的反馈：sent只是表示发出去成功, 有可能是失败
		//可以考虑加个数据过期机制
		return msg2Pushs.get(msgId);
		//return tokenPushs.remove(token);
	}
	
	public DsRouterPushId get(String msgId){
		return msg2Pushs.get(msgId);
	}
	
	public void clear(){
		this.msg2Pushs.clear();
	}
	
	public int getSize(){
		return msg2Pushs.size();
	}
	
}
