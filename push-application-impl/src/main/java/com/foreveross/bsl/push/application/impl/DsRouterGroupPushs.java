/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-9-5
 *
 */
public class DsRouterGroupPushs {
	
	private final static Logger log = LoggerFactory.getLogger(DsRouterGroupPushs.class);
	private String dsRouter;
	private List<String> pushIds;
	
	public static List<DsRouterGroupPushs> group(DsRouterPushId... pushIds){
		if(pushIds==null){
			return null;
		}
		Map<String, List<String>> map=Maps.newHashMap();
		List<String> l=null;
		for(DsRouterPushId p : pushIds){
			if(p==null){
				log.error("pushIds contain null element!!");
				continue;
			}
			l=map.get(p.getDsRouter());
			if(l==null){
				l=Lists.newArrayList(p.getPushId());
				map.put(p.getDsRouter(), l);
			}
			else{
				l.add(p.getPushId());
			}
		}
		List<DsRouterGroupPushs> result=Lists.newArrayListWithExpectedSize(map.size());
		for(Map.Entry<String, List<String>> entry : map.entrySet()){
			result.add(new DsRouterGroupPushs(entry.getKey(), entry.getValue()));
		}
		return result;
	}
	
	public DsRouterGroupPushs(String dsRouter, List<String> pushIds) {
		this.dsRouter = dsRouter;
		this.pushIds = pushIds;
	}

	/**
	 * @return the dsRouter
	 */
	public String getDsRouter() {
		return dsRouter;
	}
	/**
	 * @param dsRouter the dsRouter to set
	 */
	public void setDsRouter(String dsRouter) {
		this.dsRouter = dsRouter;
	}
	/**
	 * @return the pushIds
	 */
	public List<String> getPushIds() {
		return pushIds;
	}
	/**
	 * @param pushIds the pushIds to set
	 */
	public void setPushIds(List<String> pushIds) {
		this.pushIds = pushIds;
	}
	
}
