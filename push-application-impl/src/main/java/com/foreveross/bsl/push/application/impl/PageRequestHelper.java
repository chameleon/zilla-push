/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application.impl;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-8-16
 *
 */
public abstract class PageRequestHelper {

	/**
	 * 
	 * @param pageNumber 从1开始
	 * @param pageSize
	 * @return
	 */
	public static Pageable buildPageable(int pageNumber, int pageSize){
		pageNumber = pageNumber <= 0 ? 0 : pageNumber - 1;
		pageSize = pageSize <= 0 ? 10 : pageSize;
		Pageable pageable=new PageRequest(pageNumber, pageSize);
		return pageable;
	}
			
	public static Pageable buildPageable(int pageNumber, int pageSize, Sort sort){
		pageNumber = pageNumber <= 0 ? 0 : pageNumber - 1;
		pageSize = pageSize <= 0 ? 10 : pageSize;
		Pageable pageable=new PageRequest(pageNumber, pageSize, sort);
		return pageable;
	}		
}
