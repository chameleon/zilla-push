/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.application.impl;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.foreveross.bsl.push.application.Event;
import com.google.common.eventbus.EventBus;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-9-4
 *
 */
@Component
public class EventPostHelper {

	@Inject
	private EventBus eventBus;
	
	public void post(Event event){
		String dsRouter=DsRouterHelper.getCurrentDsRouter();
		event.setDsRouter(dsRouter);
		this.eventBus.post(event);
	}
	
	public EventBus getEventBus(){
		return eventBus;
	}
}
