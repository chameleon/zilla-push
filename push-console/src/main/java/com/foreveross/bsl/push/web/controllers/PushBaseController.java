package com.foreveross.bsl.push.web.controllers;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.context.request.WebRequest;

import com.foreveross.bsl.push.web.AjaxUtils;


public abstract class PushBaseController {
	
	public static final String PAGE_SIZE = "10";
	public static final int PAGINATION_SIZE=10;
	public static final String SEARCH_PREFIX="search_";
	public static final String SEARCH_PARAM_NAME="searchParams";
	
	@ModelAttribute()
	public void prepareCommonModel(WebRequest request, Model model){
		model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
	}
	
	public String getCurrentUserId(){
		return "anonymous";
	}
}
