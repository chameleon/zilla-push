package com.foreveross.bsl.push.web.controllers;

import javax.inject.Inject;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.foreveross.bsl.common.utils.web.QuerySpecs;
import com.foreveross.bsl.push.application.CheckinMgmtService;
import com.foreveross.bsl.push.application.CheckinService;
import com.foreveross.bsl.push.application.vo.DeviceCheckinVo;
import com.foreveross.bsl.push.web.PageMustache;

@Controller
@RequestMapping(value = "/push/dc")
public class DeviceCheckinController extends PushBaseController{
	
	@Inject
	private CheckinMgmtService checkinMgmtServiceClient;
	
	@Inject
	private CheckinService checkinServiceClient;

	@RequestMapping(value = "checkin", method = RequestMethod.GET)
	public String checkinForm(Model model){
		model.addAttribute("checkin", new DeviceCheckinVo());
		return "push/dc/checkin-form";
	}
	
	@RequestMapping(value = "checkin", method = RequestMethod.POST)
	public String checkin(DeviceCheckinVo checkin, RedirectAttributes redirectAttributes){
		this.checkinServiceClient.checkin(checkin);
		redirectAttributes.addFlashAttribute("message", "创建设备签到成功");
		return "redirect:/push/dc";
	}
	
	@RequestMapping(value = "")
	public String list(@RequestParam(value = "page", defaultValue = "1") int pageNumber,
			@RequestParam(value = "page.size", defaultValue = PAGE_SIZE) int pageSize,
			Model model){
		Page<DeviceCheckinVo> dcs=checkinMgmtServiceClient.findPageBy(pageNumber, pageSize, QuerySpecs.EMPTY_QUERY_SPECS);
		model.addAttribute("checkins", dcs);
		model.addAttribute("page", new PageMustache(dcs, PAGINATION_SIZE));
		return "push/dc/checkin-list";
	}
	
	@RequestMapping(value = "delete/{id}")
	public String delete(@PathVariable("id") String id, RedirectAttributes redirectAttributes) {
		checkinMgmtServiceClient.remove(id);
		redirectAttributes.addFlashAttribute("message", "删除成功");
		return "redirect:/push/dc";
	}
	
}
