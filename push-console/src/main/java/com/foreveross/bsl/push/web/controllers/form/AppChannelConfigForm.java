/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.web.controllers.form;

import org.springframework.web.multipart.MultipartFile;

/**
 * xxx
 * 
 * @author Wangyi
 * @version v1.0
 * 
 * @date 2013-8-15
 * 
 */
public class AppChannelConfigForm {

	private String appId;
	private ApnsConfigForm apns;
	private ApnsConfigForm apnsSandbox;
	private OpenfireConfigForm openfire;

	public static class ApnsConfigForm {
		private MultipartFile certFile;
		private String certPassword;

		/**
		 * @return the certFile
		 */
		public MultipartFile getCertFile() {
			return certFile;
		}

		/**
		 * @param certFile
		 *            the certFile to set
		 */
		public void setCertFile(MultipartFile certFile) {
			this.certFile = certFile;
		}

		/**
		 * @return the certPassword
		 */
		public String getCertPassword() {
			return certPassword;
		}

		/**
		 * @param certPassword
		 *            the certPassword to set
		 */
		public void setCertPassword(String certPassword) {
			this.certPassword = certPassword;
		}
	}

	public static class OpenfireConfigForm {
		private String username;
		private String password;

		/**
		 * @return the username
		 */
		public String getUsername() {
			return username;
		}

		/**
		 * @param username
		 *            the username to set
		 */
		public void setUsername(String username) {
			this.username = username;
		}

		/**
		 * @return the password
		 */
		public String getPassword() {
			return password;
		}

		/**
		 * @param password
		 *            the password to set
		 */
		public void setPassword(String password) {
			this.password = password;
		}

	}

	/**
	 * @return the appId
	 */
	public String getAppId() {
		return appId;
	}

	/**
	 * @param appId
	 *            the appId to set
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}

	/**
	 * @return the apns
	 */
	public ApnsConfigForm getApns() {
		return apns;
	}

	/**
	 * @param apns
	 *            the apns to set
	 */
	public void setApns(ApnsConfigForm apns) {
		this.apns = apns;
	}

	/**
	 * @return the openfire
	 */
	public OpenfireConfigForm getOpenfire() {
		return openfire;
	}

	/**
	 * @param openfire
	 *            the openfire to set
	 */
	public void setOpenfire(OpenfireConfigForm openfire) {
		this.openfire = openfire;
	}

	/**
	 * @return the apnsSandbox
	 */
	public ApnsConfigForm getApnsSandbox() {
		return apnsSandbox;
	}

	/**
	 * @param apnsSandbox
	 *            the apnsSandbox to set
	 */
	public void setApnsSandbox(ApnsConfigForm apnsSandbox) {
		this.apnsSandbox = apnsSandbox;
	}
}
