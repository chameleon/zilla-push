/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.push.web.controllers.form;

import java.util.Map;

import com.foreveross.bsl.push.application.vo.MessageVo;
import com.foreveross.bsl.push.application.vo.ReceiverTypeEnumVo;

/**
 * xxx
 * 
 * @author Wangyi
 * @version v1.0
 * 
 * @date 2013-8-16
 * 
 */
public class MessageSubmitForm {

	private ReceiverTypeEnumVo receiverType;
	private String receiverValue;
	private Map<String, String> tags;
	private String appId;
	private long timeToLive = 86400;
	private String submitUserId;
	private MessageVo message;
	private Map<String, Object> extras;
	private String moduleIdentifer;

	/**
	 * @return the receiverType
	 */
	public ReceiverTypeEnumVo getReceiverType() {
		return receiverType;
	}

	/**
	 * @param receiverType
	 *            the receiverType to set
	 */
	public void setReceiverType(ReceiverTypeEnumVo receiverType) {
		this.receiverType = receiverType;
	}

	/**
	 * @return the receiverValue
	 */
	public String getReceiverValue() {
		return receiverValue;
	}

	/**
	 * @param receiverValue
	 *            the receiverValue to set
	 */
	public void setReceiverValue(String receiverValue) {
		this.receiverValue = receiverValue;
	}

	/**
	 * @return the tags
	 */
	public Map<String, String> getTags() {
		return tags;
	}

	/**
	 * @param tags
	 *            the tags to set
	 */
	public void setTags(Map<String, String> tags) {
		this.tags = tags;
	}

	/**
	 * @return the appId
	 */
	public String getAppId() {
		return appId;
	}

	/**
	 * @param appId
	 *            the appId to set
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}

	/**
	 * @return the timeToLive
	 */
	public long getTimeToLive() {
		return timeToLive;
	}

	/**
	 * @param timeToLive
	 *            the timeToLive to set
	 */
	public void setTimeToLive(long timeToLive) {
		this.timeToLive = timeToLive;
	}

	/**
	 * @return the submitUserId
	 */
	public String getSubmitUserId() {
		return submitUserId;
	}

	/**
	 * @param submitUserId
	 *            the submitUserId to set
	 */
	public void setSubmitUserId(String submitUserId) {
		this.submitUserId = submitUserId;
	}

	/**
	 * @return the message
	 */
	public MessageVo getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(MessageVo message) {
		this.message = message;
	}

	/**
	 * @return the extras
	 */
	public Map<String, Object> getExtras() {
		return extras;
	}

	/**
	 * @param extras
	 *            the extras to set
	 */
	public void setExtras(Map<String, Object> extras) {
		this.extras = extras;
	}

	/**
	 * @return the moduleIdentifer
	 */
	public String getModuleIdentifer() {
		return moduleIdentifer;
	}

	/**
	 * @param moduleIdentifer the moduleIdentifer to set
	 */
	public void setModuleIdentifer(String moduleIdentifer) {
		this.moduleIdentifer = moduleIdentifer;
	}

}
