/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl.cmd;

import com.foreveross.bsl.Command;
import com.foreveross.bsl.Config;
import com.foreveross.bsl.MessageStatistics;
import com.foreveross.bsl.openfire.OpenfireClient;
import com.foreveross.bsl.push.OpenfirePushClient;
import com.foreveross.bsl.push.PushClient;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-12-10
 *
 */
public class OpenfirePushCmd extends OpenfireMsgReceiveCmd implements Command {
	
	
	/**
	 * @param msgStat
	 * @param openfireHost
	 * @param port
	 * @param clientCount
	 * @param from
	 */
	public OpenfirePushCmd(MessageStatistics msgStat, Config config) {
		super(msgStat, config);
	}
	
	/* (non-Javadoc)
	 * @see com.foreveross.bsl.cmd.OpenfireMsgReceiveCmd#doCreateClient(java.lang.String, java.lang.String)
	 */
	@Override
	protected void doCreateClient(String username, String password) throws Exception {
		OpenfireClient openfirec=new OpenfireClient(this.getHost(), this.getPort());
		openfirec.setUsername(username);
		openfirec.setPassword(password);
		
		PushClient pc=new OpenfirePushClient(this.getMessageStatistics(), openfirec, this.getPushUrl());
		pc.setAppId(this.getAppId());
		pc.setDeviceId(username);
		
		pc.checkin();
	}
	
	public String getPushUrl(){
		return this.getConfig().getProperty("push.url", true);
	}
	
	public String getAppId(){
		return this.getConfig().getProperty("push.appId", true);
	}

}
