/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-12-6
 *
 */
public interface Command {
	
	void execute();
	
	final Command EMPTY_CMD=new Command(){
		@Override
		public void execute() {
		}
	};
}
