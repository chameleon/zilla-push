package com.foreveross.bsl.push;

import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.foreveross.bsl.PushMessage;

/**
 * 推送请求的消息参数信息
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-7-12
 *
 */
@XmlRootElement(name = "MessageParams")
public class MessageParams {

	/**
	 * 消息接收者范围选择类型，不能为空
	 */
	private ReceiverTypeEnum receiverType;

	/**
	 * 对应消息接收者范围选择类型的值，当类型为APP_ID,TAG时，可为空
	 */
	private String receiverValue;
	
	/**
	 * 当receiverType为TAG时tags表示用户输入的标签集。
	 * 使用key-value表示，key为标签的键，value为标签的值，多个值使用“,”分隔。
	 * 查找时，对多个标签键做AND操作,标签键内的多值做OR操作
	 */
	private Map<String, String> tags;
	
	/**
	 * 推送消息的应用ID
	 */
	private String appId;
	
	/**
	 * 保存离线的时长。秒为单位。最多支持10天（864000秒）。
	 * 0 表示该消息不保存离线。即：用户在线马上发出，当前不在线用户将不会收到此消息。
	 * 此参数不设置则表示默认，默认为保存1天的离线消息（86400秒）。	
	 */
	private long timeToLive=86400; 
	
	/**
	 * 推送提交的租户ID
	 */
	private String submitUserId;

	/**
	 * 实际的消息
	 */
	private PushMessage message = new PushMessage();

	/**
	 * @return the receiverType
	 */
	public ReceiverTypeEnum getReceiverType() {
		return receiverType;
	}

	/**
	 * @param receiverType the receiverType to set
	 */
	public void setReceiverType(ReceiverTypeEnum receiverType) {
		this.receiverType = receiverType;
	}
	
	@JsonIgnore
	public boolean isByTag(){
		return ReceiverTypeEnum.TAG.equals(this.getReceiverType());
	}
	
	@JsonIgnore
	public boolean isByApp(){
		return ReceiverTypeEnum.APP_ID.equals(this.getReceiverType());
	}
	
	@JsonIgnore
	public boolean isByDevice(){
		return ReceiverTypeEnum.DEVICE_ID.equals(this.getReceiverType());
	}
	
	@JsonIgnore
	public boolean isByAlias(){
		return ReceiverTypeEnum.ALIAS.equals(this.getReceiverType());
	}
	
	public void validate(){
		if(StringUtils.isBlank(this.appId)){
			throw new IllegalArgumentException("appId属性不能为空");
		}
		if(this.receiverType==null){
			throw new IllegalArgumentException("receiverType属性不能为空");
		}
		if(this.message==null){
			throw new IllegalArgumentException("message属性不能为空");
		}
	}

	/**
	 * @return the receiverValue
	 */
	public String getReceiverValue() {
		if(this.isByApp()){
			this.receiverValue=appId;
		}
		return receiverValue;
	}

	/**
	 * @param receiverValue the receiverValue to set
	 */
	public void setReceiverValue(String receiverValue) {
		this.receiverValue = receiverValue;
	}

	/**
	 * @return the appId
	 */
	public String getAppId() {
		return appId;
	}

	/**
	 * @param appId the appId to set
	 */
	public void setAppId(String appId) {
		this.appId = appId;
		if(this.isByApp()){
			this.receiverValue=appId;
		}
	}

	/**
	 * @return the timeToLive
	 */
	public long getTimeToLive() {
		return timeToLive;
	}

	/**
	 * @param timeToLive the timeToLive to set
	 */
	public void setTimeToLive(long timeToLive) {
		this.timeToLive = timeToLive;
	}

	/**
	 * @return the submitUserId
	 */
	public String getSubmitUserId() {
		return submitUserId;
	}

	/**
	 * @param submitUserId the submitUserId to set
	 */
	public void setSubmitUserId(String submitUserId) {
		this.submitUserId = submitUserId;
	}

	/**
	 * @return the message
	 */
	public PushMessage getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(PushMessage message) {
		this.message = message;
	}

	/**
	 * @return the tags
	 */
	public Map<String, String> getTags() {
		return tags;
	}

	/**
	 * @param tags the tags to set
	 */
	public void setTags(Map<String, String> tags) {
		this.tags = tags;
	}

}
