package com.foreveross.bsl.apns;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ApnsTest {

	private final static Logger log = LoggerFactory.getLogger(ApnsTest.class);
	
	private final Map<String, CertApnsService> certApnsServices=new HashMap<String, CertApnsService>();
	
	public void run(boolean useProxy) throws Exception {
		Proxy proxy=null;
		if(useProxy){
			proxy=this.getProxy();
			log.debug("use proxy: {}", proxy);
		}
		buildService(proxy);
		//testPushToImpcMulti();
		testPushToSocMulti();
	}
	
	public static void main(String[] args) throws Exception{
		ApnsTest at=new ApnsTest();
		at.run(false);
		Thread.sleep(10000);
	}
	
	protected Proxy getProxy(){
		SocketAddress addr = new InetSocketAddress("172.16.65.11", 18570);
		return new Proxy(Proxy.Type.SOCKS, addr);
	}
	
	public void testPushToImpcMulti() throws Exception {
		String tokens[] = {
				"E248C89B4994C68D8C9EE0C03B22A716A6394CDE8E2DCF37C647092B937B7A88",
				"A852C05FBC05265DCC772B06AFFAA4EA4934FB3A042C1010FE5FFF3773B87E98",
				"234CA636F14A7A422B926B3170B0C735ACE24522EF3CE861247A3015E64403A3",
				"09E9FC19650DF345269F4028F902C3CED78A5094C2A39F62890F85DC5623F54C",
				"509D75A54935365340D01A8EA4087519345875012DE09AD5396B71DA2BEEF362"
			};
		String certfile = "impc_push_dist.p12";
		log.debug("--------------开始推送到南航移动应用---------------");
		CertApnsService cas=getCertApnsService(certfile);
		cas.send(tokens, "Hello Test!", 5);
		log.debug("Total:{}, sent:{}, failed:{}", cas.getTotalCount(), cas.getTotalSent(), cas.getTotalFailed());
	}
	
	public void testPushToSocMulti() throws Exception {
		String tokens[] = {
//				"44FD6ED7AF9202F9FAFB9FCFF83F5C651AF2337EC3807F71EF9B84C00573B27A", //6A
//				"B5FC4AA99D6456D0F86FC790EC70D5EB304A57D529BE33905F1A68EB94745897"
				"9AE9B6BCD112D0FE5F0A99278FFA4F3E728EDC92DE806DA2C7F438D12757C983"
			};
		String certfile = "dev_chameleon.p12";
		log.debug("--------------开始推送到移动运行网应用---------------");
		CertApnsService cas=getCertApnsService(certfile);
		long start=System.currentTimeMillis();
		cas.send(tokens, "Hello Test!", 5);
		log.debug("Total:{}, sent:{}, failed:{}", cas.getTotalCount(), cas.getTotalSent(), cas.getTotalFailed());
		log.debug("Cost time: {}ms", System.currentTimeMillis()-start);
	}
	
	private void buildService(Proxy proxy) throws IOException{
		String certfile = "dev_chameleon.p12";
		String certPassword = "123456";
		certApnsServices.put(certfile, new CertApnsService(certfile, certPassword, proxy, true));
	}
	
	protected CertApnsService getCertApnsService(String certFilename){
		return certApnsServices.get(certFilename);
	}
	
}
