/**
 * Copyright (C) 2013-2014 the original author or authors.
 */
package com.foreveross.bsl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;

/**
 * xxx
 *
 * @author Wangyi
 * @version v1.0
 *
 * @date 2013-12-11
 *
 */
public class Config {

	private final Properties props=new Properties();
	
	public Config(String filepath) throws FileNotFoundException, IOException{
		File file = new File(filepath);
		if(!file.isAbsolute()){
			file=new File(System.getProperty("user.dir"), filepath);
		}
		props.load(new FileInputStream(file));
	}
	
	public Properties getProperties(){
		return this.props;
	}

	public String getProperty(String key){
		return this.props.getProperty(key);
	}
	
	public String getProperty(String key, boolean checkNotBlank){
		String value=this.props.getProperty(key);
		if(checkNotBlank && StringUtils.isBlank(value)){
			throw new IllegalArgumentException("配置"+key+"为空");
		}
		return value;
	}
	
	public String getProperty(String key, String defaultValue){
		return this.props.getProperty(key, defaultValue);
	}

}
